<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:host=127.0.0.1;dbname=oblig1;charset=utf8mb4', 'root', 'root');
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		$stmt = $this->db->query( 'SELECT * FROM Book ORDER BY id');
		
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			$booklist[] = (object)$booklist;
		}
		
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$stmt = $this->db->prepare('SELECT * FROM Book WHERE id=?');
		$stmt = execute([$id]);
		$book = $stmt->fetch(PDO::FETCH_ASSOC);
		
		if (is_scalar($book)) return null;
        return (object)$book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		$stmt = $this->db->prepare('INSERT INTO Book (title, author, description)
		VALUES (?,?,?)');
		$stmt->execute([$book->title, $book->author, $book->description]);
		
		$book->id = $this->db->lastInsertId();		
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		$stmt = $this->db->prepare( 'UPDATE Book SET title=?, author=?, description=? WHERE id=?'); 
		$stmt->execute([$book->title, $book->author, $book->description, $book->id]);
		
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		$stmt = $this->db->prepare('DELETE FROM Book WHERE id=?');
		$stmt->execute([$id]);
    }
	
}

?>